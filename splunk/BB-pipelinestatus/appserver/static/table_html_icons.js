require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/simplexml/ready!'
], function (_, $, mvc, TableView, themeUtils) {
    var HtmlImgIconRenderer = TableView.BaseCellRenderer.extend({
        canRender: function (cell) {
            // Only use the cell renderer for the range field
            return cell.field === 'icon';
        },
        render: function ($td, cell) {
            // Create the icon element and add it to the table cell
            $td.html(_.template('<img class="bb-avatar" src="<%-imgsrc%>">', {
                imgsrc: cell.value
            }))
        }
    });
    mvc.Components.get('table1').getVisualization(function (tableView) {
        // Register custom cell renderer, the table will re-render automatically
        tableView.addCellRenderer(new HtmlImgIconRenderer());
    });
});