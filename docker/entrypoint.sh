#!/usr/bin/env bash
set -e          # Abort script at first error, when a command exits with non-zero status 
set -o pipefail # Causes a pipeline to return the exit status of the last command in the 
                # pipe that returned a non-zero return value.
set -u          # Attempt to use undefined variable outputs error message, and forces an exit

DEBUG=${DEBUG:-}
if [[ ! -z $DEBUG ]]; then
	set -x
fi


# env vars
##
# Bitbucket authentication, in the form of <username>:<password>
# Best practice is to create an app password for this operation
BB_AUTH=${BB_AUTH:-}

##
# Bitbucket workspace name
BB_WORKSPACE=${BB_WORKSPACE:-}

##
# Splunk HEC hostname
# The hostname which bitbucket can use to connect to your splunk HEC
# This must hold just the hostname, so it should NOT start with https://
SPLUNK_HEC_HOST=${SPLUNK_HEC_HOST:-}

## 
# Splunk HEC port
# Port of your splunk HEC, defaults to 8088 
SPLUNK_HEC_PORT=${SPLUNK_HEC_PORT:-8088}

##
# Splunk HEC token to use to submit data to your HEC
SPLUNK_HEC_TOKEN=${SPLUNK_HEC_TOKEN:-}

##
# Should Bitbucket verify the certificate on your HEC
# defaults to false, setting this to any other string will set this to true
VERIFY_CERT=${VERIFY_CERT:-false}


# internal vars
BB_BASE_URL="https://api.bitbucket.org/2.0/repositories/"
CURL_OPTS="-s -L -u $BB_AUTH --basic"
HOOK_NAME="splunk-pipeline-status"
HOOK_URL="https://x:$SPLUNK_HEC_TOKEN@$SPLUNK_HEC_HOST:$SPLUNK_HEC_PORT/services/collector/raw"
if [[ $VERIFY_CERT = "false" ]]; then
	NO_VERIFY_CERT="true"
else 
	NO_VERIFY_CERT="false"
fi
MAX_RECURSION=10


# $1 full url
# $2 recursion counter (optional)
function get_repo_slugs {
	COUNTER=${2:-1}
	if [[ $COUNTER -eq 1 ]]; then
		echo -n "" > repos
	fi
	if [[ $COUNTER -gt $MAX_RECURSION ]]; then
		echo " [x] Maximum number of repos reached"
		return
	fi
	echo " [-] Retrieve repos from $1"
	curl $CURL_OPTS -X GET -o out $1
	NEXT=$(cat out | jq -r '.next' | grep -v "null" || echo -n "")
	cat out | jq -r '.values | .[] | .slug' >> repos
	
	if [ ! -z $NEXT ]; then
		COUNTER=$((COUNTER+1))
		get_repo_slugs $NEXT $COUNTER
	fi
}

# $1 full url
# $2 slug
# $3 recursion counter (optional)
function get_hooks_for {
	COUNTER=${3:-1}
	if [[ $COUNTER -eq 1 ]]; then
		echo -n "" > $2.hook_descriptions
	fi
	if [[ $COUNTER -gt $MAX_RECURSION ]]; then
		echo " [x] Maximum number of hooks reached"
		return
	fi
	echo " [-] Getting existing hooks for $2"
	curl $CURL_OPTS -X GET -o $2.out $1
	NEXT=$(cat $2.out | jq -r '.next' | grep -v "null" || echo -n "")
	cat $2.out | jq -r '.values | .[] | .description' >> $2.hook_descriptions

	if [ ! -z $NEXT ]; then
		COUNTER=$((COUNTER+1))
		get_hooks_for $NEXT $2 $COUNTER
	fi
}

# $1 full url
# $2 slug
function add_hook_to {
	if grep -q '^'$HOOK_NAME'$' $2.hook_descriptions ; then
		echo " [x] Hook already exists in $2, nothing to do"
		return
	fi
	echo " [*] Add hook to $2"
	CODE=$(curl $CURL_OPTS -X POST -w '%{http_code}' -H 'Content-Type: application/json' $1 -o $2.hook -d "
	{
		\"description\":\"$HOOK_NAME\",
		\"url\":\"$HOOK_URL\",
		\"active\":\"true\",
		\"skip_cert_verification\":$NO_VERIFY_CERT,
		\"events\": [
    		\"repo:commit_status_updated\",
    		\"repo:commit_status_created\"
  		]
	}
	")
	if [[ $CODE -eq 201 ]]; then
		echo " [+] Hook added to $2";
	else 
		echo " [x] Error adding hook to $2. [$CODE - see $2.hook]"
	fi
}

echo "[*] Get all repository names"
get_repo_slugs $BB_BASE_URL$BB_WORKSPACE/
REPO_COUNT=$(wc -l repos | cut -d " " -f1)
echo "[+] Retrieved $REPO_COUNT repositories"

echo "[*] Adding webhooks"
I=1
while read -r slug ; do
	echo "[$I/$REPO_COUNT] $slug"
	get_hooks_for $BB_BASE_URL$BB_WORKSPACE/$slug/hooks/ $slug
	add_hook_to $BB_BASE_URL$BB_WORKSPACE/$slug/hooks/ $slug
	I=$((I+1))
done < repos

