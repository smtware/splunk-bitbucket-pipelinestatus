# Bitbucket pipeline status

Setup bitbucket to sent real-time status of pipelines to splunk.

This image will add a webhook to all repositories it is granted access to. This webhook will send the status of piplines to the splunk HEC.

# Variables

In order to run setup bitbucket you have to privde the following environment varibales to this image:

```
BB_AUTH

# Bitbucket authentication, in the form of <username>:<password>
# Best practice is to create an app password for this operation


BB_WORKSPACE

# Bitbucket workspace name


SPLUNK_HEC_HOST

# Splunk HEC hostname
# The hostname which bitbucket can use to connect to your splunk HEC
# This must hold just the hostname, so it should NOT start with https://


SPLUNK_HEC_PORT

# Splunk HEC port
# Port of your splunk HEC, defaults to 8088 


SPLUNK_HEC_TOKEN

# Splunk HEC token to use to submit data to your HEC



VERIFY_CERT

# Should Bitbucket verify the certificate on your HEC
# defaults to false, setting this to any other string will set this to true

```

*This image will try to add the webhook to all repositories the provided authentication has access to.*

You can add these varibles to a file and tell docker to use the contents of this file as environment variables. Create a file called ```env-file``` with a content like this:

```
BB_AUTH=johnusername:ADue45GJjfH4Eus8%dnf
BB_WORKSPACE=awesomeus
SPLUNK_HEC_HOST=hec.splunk.awesomeus.example
SPLUNK_HEC_TOKEN=307eb7c5-55fe-4323-9408-e60f67f659bd
```

# Note

Error handling is very minimal. Software is provided AS-IS. Use at your own risk.

# building and running

Build

```
docker build -t bbhooks:1 .
```

Run

```
docker run --env-file env-file bbhooks:1
```