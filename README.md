# Splunk Bitbucket pipeline status

Do you want to be able to see the status of your bitbucket pipelines in Splunk? This repository contains the tools to achieve exactly that.

We provide a Splunk app which contains:

- a dashboard to view the current pipeline status of all repositories and branches and 
- a dashboard to compare the latest build time with the history of builds for that branch
- the configuration for the Splunk HEC to receive the updates from bitbucket

We provide a docker image to configure bitbucket to sent the needed data to bitbucket.

# Splunk app

The directory ```splunk``` contains the splunk app. You can either copy the ```BB-pipelinestatus``` directory to ```$SPLUNK_HOME/etc/apps``` or create a .tgz from the ```BB-pipelinestatus``` directory and add this as a new app to your Splunk environment.

# docker image

This solution uses a bitbucket webhook to sent data to Splunk. In order to setup the webhook in all your repositories we provide a docker image for your conveinience. When run a container will connect to your bitbucket environment and will try to add the nececary webhook to **all** your repositories. See the README.md file in the ```docker``` directory for further details.